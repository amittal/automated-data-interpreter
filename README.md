# Automated Data Interpretor
##### Adhishwar Singh Mittal    
amittal@tcd.ie    
**Keywords**: Abalone Age Prediction, Genetic Algorithm, Auto-ML, Regression, LightGBM

> This project is the 2nd step in building an Auto-ML. Previously I worked on creating a way to do a random search over a group of macro hyperparamaters in my project "The Big Black Box". It seemed that I could get good results with random searches as well. But the algorithm had no way to optimize for the best results. Therefore, it never converged. Moreover, the methods need great deal of time to reach an acceptable result as the number of parameters and thus the search space grew.
Therefor, I decided to create an optimization algorithm for that using genetic algorithm which uses paramater values to create random samples and then uses stronger samples to create offsprings which leads to the evolutionary growth. The algorothm outputs the best configuration at the end as found using the available serach state space.

For complete article follow - https://medium.com/@amittal_84508/auto-ml-using-genetic-algorithm-for-regression-tasks-8b6048fb6344